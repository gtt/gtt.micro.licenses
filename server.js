var http = require('http');
var send = require('send');
var port = (process.env.PORT || 1337);

var app = http.createServer(function(req, res){
  send(req, req.url).from(__dirname).pipe(res);
}).listen(port);
console.log("listening on port " + port);